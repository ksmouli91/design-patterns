package com.ks.mouli.design.pattern.behaviour.command;

public class MyWorkFlowMain {
	public static void main(String args[]) {
		WorkFlowcontrol worFlow = new WorkFlowcontrol();
		Light light = new Light();
		Laptop laptop = new Laptop();
		
		worFlow.setCommand(new LightOnCommand(light));
		worFlow.startActivity();
		
		worFlow.setCommand(new WorkingTimeCommand(laptop));
		worFlow.startActivity();
		
		worFlow.setCommand(new NonWorkingTimeCommand(laptop));
		worFlow.startActivity();
		
		worFlow.setCommand(new LightOffCommand(light));
		worFlow.startActivity();

	}
}
