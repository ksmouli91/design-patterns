package com.ks.mouli.design.pattern.openClosePrincipal.shapeExample;

import java.util.List;

public class AreaCalculator {
	public double calculateArea(List<Object> shapes) {
		double area = 0;
		for (Object shape : shapes) {
			if (shape instanceof Rectangle) {
				Rectangle rect = (Rectangle) shape;
				area = area + (rect.length * rect.height);
			} else if (shape instanceof Circle) {
				Circle circle = (Circle) shape;
				area = area + (circle.radius * circle.radius * Math.PI);
			} else {
				throw new RuntimeException("This shape not recognized");
			}
		}
		return area;
	}
}