package com.ks.mouli.design.pattern.behaviour.decorator;
public interface Shape {
   void draw();
}