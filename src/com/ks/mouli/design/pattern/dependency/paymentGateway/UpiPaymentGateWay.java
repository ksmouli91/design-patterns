package com.ks.mouli.design.pattern.dependency.paymentGateway;

public interface UpiPaymentGateWay {

	void doPayment();
	
	void getOffer();
}

