package com.ks.mouli.design.pattern.kissPrincipal;

//all are required 
public class BankAccountCreation {

	public Boolean accountRegistrationValidation(BankAccountUser registrationForm) {
		if (registrationForm.aadharNum == null ) return false;
		if (registrationForm.panCard == null ) return false;
		if (registrationForm.mobileNumber == null ) return false;
		if (registrationForm.emailAddress == null ) return false;
		return true;
	}
	
	
	public Boolean registrationValidation(BankAccountUser registrationForm) {
		if (registrationForm.aadharNum != null && registrationForm.panCard != null
				&& registrationForm.mobileNumber != null && registrationForm.emailAddress != null) {
			return true;
		}
		return false;
	}
}
