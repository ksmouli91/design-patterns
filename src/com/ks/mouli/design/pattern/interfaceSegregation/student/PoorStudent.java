package com.ks.mouli.design.pattern.interfaceSegregation.student;

public class PoorStudent implements WriteActivityStudent , ReadActivityStudent {

	@Override
	public void read() {
		System.out.println("You must read ");
	}

	@Override
	public void write() {
		System.out.println("You must write ");
	}

	
	
}
