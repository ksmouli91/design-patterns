package com.ks.mouli.design.pattern.singleResponsibility.employeeExmaple;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EmployeeMain {

	public static void main(String[] args) throws ParseException {
        String date = "12-20-2016";
        Double salary = 20000.0;
		EmployeeExample emp = new EmployeeExample();
        String pattern = "MM-dd-yyyy";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
		Date date1 = format.parse(date);
		emp.isHikeReviewDue(date1);
		//====================//
		emp.creditSalary(salary);
		
		EmployeeHikeCheck hikeCheck = new EmployeeHikeCheck();
		hikeCheck.isHikeReviewDue(date1);
		
		CreditEmployeeSalary employeeSalary =  new CreditEmployeeSalary();
		employeeSalary.creditSalary(salary);
		
	}

}
