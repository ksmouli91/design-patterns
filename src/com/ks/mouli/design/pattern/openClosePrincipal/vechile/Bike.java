package com.ks.mouli.design.pattern.openClosePrincipal.vechile;

public class Bike extends Vehicle {

	public int noOfWheels = 2;
	
	public int getNoOfWheels() {
		return this.noOfWheels;
	}
}
