package com.ks.mouli.design.pattern.interfaceSegregation.student;

public class TopStudent implements LeadActivityStudent {

	@Override
	public void lead() {
		System.out.println("You have to lead ..");
	}

}
