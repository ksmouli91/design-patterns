package com.ks.mouli.design.pattern.behaviour.decorator;
public class Circle implements Shape {

   @Override
   public void draw() {
      System.out.println("Shape: Circle");
   }
}