package com.ks.mouli.design.pattern.openClosePrincipal.shapeExample;

public interface Shape {
	double calculateArea();
}