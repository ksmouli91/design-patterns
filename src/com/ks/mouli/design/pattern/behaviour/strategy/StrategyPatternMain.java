package com.ks.mouli.design.pattern.behaviour.strategy;

public class StrategyPatternMain {
	public static void main(String[] args) {
		Context context = new Context(new Add());
		System.out.println("91 + 9 = " + context.runStrategy(91, 9));

		context = new Context(new Substract());
		System.out.println("91 - 9 = " + context.runStrategy(91, 9));

		context = new Context(new Multiply());
		System.out.println("91 * 9 = " + context.runStrategy(91, 9));
	}
}