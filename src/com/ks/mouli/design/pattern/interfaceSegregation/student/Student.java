package com.ks.mouli.design.pattern.interfaceSegregation.student;

public interface Student {

	void read();
	
	void write();
	
	void lead();
	
}
