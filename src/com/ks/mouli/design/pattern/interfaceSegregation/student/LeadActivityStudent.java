package com.ks.mouli.design.pattern.interfaceSegregation.student;

public interface LeadActivityStudent {

	void lead();
}
