package com.ks.mouli.design.pattern.interfaceSegregation.human;

public interface Human {

	void walk();
	
	void talk();
	
	void watch();
	
	
}
