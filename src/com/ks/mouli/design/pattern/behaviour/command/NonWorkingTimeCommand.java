package com.ks.mouli.design.pattern.behaviour.command;

public class NonWorkingTimeCommand implements Command {
	Laptop laptop;

	public NonWorkingTimeCommand(Laptop laptop) {
		this.laptop = laptop;
	}

	public void execute() {
		laptop.off();
	}
}
