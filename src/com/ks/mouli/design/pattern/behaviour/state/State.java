package com.ks.mouli.design.pattern.behaviour.state;
public interface State {
   public void doAction(Context context);
}