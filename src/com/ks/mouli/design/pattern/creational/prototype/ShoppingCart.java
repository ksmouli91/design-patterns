package com.ks.mouli.design.pattern.creational.prototype;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart implements Cloneable {

	private String itemName;
	
	private double price;
	
	private String develieryAddress;

	public void setDevelieryAddress(String develieryAddress) {
		this.develieryAddress = develieryAddress;
	}

	private List<ShoppingCart> mycart = new ArrayList<>();

	public ShoppingCart(String itemName, double price, String develieryAddress) {
		this.itemName = itemName;
		this.price = price;
		this.develieryAddress = develieryAddress;
	}

	public ShoppingCart(List<ShoppingCart> mycart) {
		this.mycart = mycart;
	}
	
	public List<ShoppingCart> getMycart() {
		return mycart;
	}

	public void myPrivouesOrder() {
		mycart.add(new ShoppingCart("chicken briyani", 200.0, "home address"));
		mycart.add(new ShoppingCart("veg briyani", 100.0, "office address"));
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException{
		List<ShoppingCart> prevOrder = new ArrayList<>();
		for(ShoppingCart c : this.getMycart()){
			prevOrder.add(c);
		}
		return prevOrder;
	}

	@Override
	public String toString() {
		return "ShoppingCart [itemName=" + itemName + ", price=" + price + ", develieryAddress=" + develieryAddress
				+ "]";
	}
	
}
