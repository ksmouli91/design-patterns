package com.ks.mouli.design.pattern.behaviour.template;

public abstract class SocialWebiteCommonTemplate {

	public final void buildWebsite(String name) {
		sendMsg();
		recieveMsg();
		videoCall();
		System.out.println (name + "features added !!");

	}

	public abstract void videoCall();

	private void sendMsg() {
		System.out.println("send msg using socket ");
	}

	private void recieveMsg() {
		System.out.println("send recieve using socket ");
	}
}
