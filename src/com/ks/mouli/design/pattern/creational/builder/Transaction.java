package com.ks.mouli.design.pattern.creational.builder;

public class Transaction {

	private final Long txnId;
	
	private final String txnNumber;
	
	private final double totalAmount;
	
	private final String address;
	
	private final String customerName;

	public Transaction(Transactionbuilder transactionbuilder) {
		super();
		this.txnId = transactionbuilder.txnId;
		this.txnNumber = transactionbuilder.txnNumber;
		this.totalAmount = transactionbuilder.totalAmount;
		this.address = transactionbuilder.address;
		this.customerName = transactionbuilder.customer;
	}

	
	@Override
	public String toString() {
		return "Transaction [txnId=" + txnId + ", txnNumber=" + txnNumber + ", totalAmount=" + totalAmount
				+ ", address=" + address + ", customerName=" + customerName + "]";
	}


	public Long getTxnId() {
		return txnId;
	}

	public String getTxnNumber() {
		return txnNumber;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public String getAddress() {
		return address;
	}

	public String getCustomerName() {
		return customerName;
	}
	
	public static class  Transactionbuilder {
		
		private final Long txnId;
		
		private final String txnNumber;
		
		private final double totalAmount;
		
		private String address;
		
		private String customer;

		public Transactionbuilder(String txnNumber, long txnId,double totalAmount) {
			this.txnNumber = txnNumber;
			this.txnId = txnId;
			this.totalAmount = totalAmount;
		}
		
		
		public Transactionbuilder customer(String customer) {
			 this.customer = customer;
			 return this;
		}
		
		public Transactionbuilder address(String address) {
			 this.address = address;
			 return this;
		}


		public Transaction build() {
			Transaction txn = new Transaction(this);
			return txn;
		}
		
	}
	
	
}
