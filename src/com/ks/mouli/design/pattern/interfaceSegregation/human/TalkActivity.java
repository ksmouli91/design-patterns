package com.ks.mouli.design.pattern.interfaceSegregation.human;

public interface TalkActivity {

	void talk();
	
}
