package com.ks.mouli.design.pattern.openClosePrincipal.shapeExample;

public class CircleShape implements Shape {
    private double radius; 

    @Override
    public double calculateArea() {
        return (radius * radius * Math.PI);
    }
}
