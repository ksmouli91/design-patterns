package com.ks.mouli.design.pattern.creational.factory;

public class House implements Building {
	@Override
	public void build() {
		System.out.println("Building House");
	}
}