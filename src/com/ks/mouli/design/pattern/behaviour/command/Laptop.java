package com.ks.mouli.design.pattern.behaviour.command;

public class Laptop {
	public void on() {
		System.out.println("Laptop is on");
	}

	public void off() {
		System.out.println("Laptop is off");
	}
	public void work() {
		System.out.println("do work in Laptop");
	}
}
