package com.ks.mouli.design.pattern.mvc.employeeApp;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDemo {

	public static void main(String[] args) {
		List<EmployeeModel> employees = getEmployees();
		EmployeeView view = new EmployeeView();
		EmployeeController employeeController = new EmployeeController(employees, view);
		employeeController.sendDataToview();
	}

	private static List<EmployeeModel> getEmployees() {
		List<EmployeeModel> employees = new ArrayList<>();
		EmployeeModel emp1 = new EmployeeModel(1l,"mouli",100.0,"java developer");
		EmployeeModel emp2 = new EmployeeModel(2l,"nadan",200.0,"front end developer");
		employees.add(emp1);
		employees.add(emp2);
		return employees;
	}

}
