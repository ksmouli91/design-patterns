package com.ks.mouli.design.pattern.creational.factory;

public class Home implements Building {
	@Override
	public void build() {
		System.out.println("Building Home");
	}
}