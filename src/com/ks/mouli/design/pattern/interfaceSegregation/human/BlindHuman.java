package com.ks.mouli.design.pattern.interfaceSegregation.human;

public class BlindHuman implements TalkActivity , WalkActivity{

	@Override
	public void walk() {
		System.out.println("he / she can walk ");
	}

	@Override
	public void talk() {
		System.out.println("he / she can talk");
	}

}
