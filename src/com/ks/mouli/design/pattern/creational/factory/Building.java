package com.ks.mouli.design.pattern.creational.factory;

public interface Building {
	void build();
}