package com.ks.mouli.design.pattern.interfaceSegregation.human;

public interface WalkActivity {

	void walk();
	
}
