package com.ks.mouli.design.pattern.behaviour.visitor;

public interface ItemElement {

	public int accept(ShoppingCartVisitor visitor);
}