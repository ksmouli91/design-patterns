package com.ks.mouli.design.pattern.dependency.paymentGateway;

public class GpayPayment {

	protected String  processPayment() {
		return "done payment with g-pay";
	}
}
