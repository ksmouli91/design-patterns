package com.ks.mouli.design.pattern.openClosePrincipal.Calculator;

public interface GenericeCalculatorOperation {
    void perform();
}
