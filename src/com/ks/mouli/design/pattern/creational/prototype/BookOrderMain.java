package com.ks.mouli.design.pattern.creational.prototype;

import java.util.ArrayList;
import java.util.List;

public class BookOrderMain {

	public static void main(String args[]) throws CloneNotSupportedException {
		ShoppingCart newOrder = new ShoppingCart(new ArrayList<>());
		newOrder.myPrivouesOrder();

		List<ShoppingCart> newOrderForHome = (List<ShoppingCart>) newOrder.clone();
		List<ShoppingCart> newOrderForOffice = (List<ShoppingCart>) newOrder.clone();

		for (ShoppingCart cart : newOrderForHome) {
			cart.setDevelieryAddress("home address");
		}

		for (ShoppingCart cart : newOrderForOffice) {
			cart.setDevelieryAddress("office address");
		}
		// place order 1 
		// place order 2
		System.out.println(newOrderForHome);
		System.out.println(newOrderForOffice);
	}
}
