package com.ks.mouli.design.pattern.creational.builder;

import com.ks.mouli.design.pattern.creational.builder.Transaction.Transactionbuilder;

public class TransactionMain {

	public static void main(String[] args) {
		
		Transaction txn = new Transactionbuilder("2021-05-20",10001l,4000)
				.address("ap").customer("mouli").build();
		System.out.println(txn);
	}

}
