package com.ks.mouli.design.pattern.singleResponsibility.employeeExmaple;

import java.util.Date;

public class EmployeeHikeCheck {

	public boolean isHikeReviewDue(Date joiningDate) {
		Long diff =   new Date().getTime() - joiningDate.getTime();
        long years_difference = (diff / (1000l*60*60*24*365));   
        System.out.println(years_difference);
        if (years_difference > 1) {
            System.out.println("yes hike is due");
        	return true;
        }
        System.out.println("no hike due");
		return false;
	}
}

