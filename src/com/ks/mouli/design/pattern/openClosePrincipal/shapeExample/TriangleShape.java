package com.ks.mouli.design.pattern.openClosePrincipal.shapeExample;

public class TriangleShape implements Shape {

	public double length;
	public double height;
	
	public TriangleShape(double length, double height) {
		this.length = length;
		this.height = height;
	}

	@Override
	public double calculateArea() {
		return 0.5 * (length * height);
	}
}
