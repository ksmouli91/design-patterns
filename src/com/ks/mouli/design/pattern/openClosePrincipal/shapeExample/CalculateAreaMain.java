package com.ks.mouli.design.pattern.openClosePrincipal.shapeExample;

import java.util.ArrayList;
import java.util.List;

public class CalculateAreaMain {

	public static void main(String[] args) {
		AreaCalculator areaCalculator = new AreaCalculator();
		List<Object> shapes =  new ArrayList<>();
		shapes.add(new Rectangle(10, 20));
		shapes.add(new Circle(20.0));
		shapes.add(new TriangleShape(20, 10));
		areaCalculator.calculateArea(shapes);
	}
}
