package com.ks.mouli.design.pattern.mvc.employeeApp;

import java.util.List;

public class EmployeeController {

	private List<EmployeeModel> employeeModel;

	private EmployeeView employeeView;

	public EmployeeController(List<EmployeeModel> model, EmployeeView view) {
		this.employeeModel = model;
		this.employeeView = view;
	}

	public void sendDataToview() {
		employeeView.showEmpoyeeDetails(employeeModel);
	}
}
