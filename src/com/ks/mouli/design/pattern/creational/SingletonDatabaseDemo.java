package com.ks.mouli.design.pattern.creational;

public class SingletonDatabaseDemo {
	public static void main(String args []) {
		System.out.println(MyDataBase.getMyDataBaseInstance());
		System.out.println(MyDataBase.getMyDataBaseInstance());
		System.out.println(MyDataBase.getMyDataBaseInstance());
	}
}

class MyDataBase {
	
	//step 1 Private constructor to restrict instantiation of the class from other classes
	private MyDataBase() {
		this.driver = "oracle:thin";
		this.userName = "system";
		this.password = "password";
	}
	//Step 2 Private static variable of the same class that is the only instance of the class
	private static MyDataBase myDataBase;
	private static int count = 0;
	//
	private  String driver;
	private  String userName ;
	private  String password ;
	

	// step 3 Public static method that returns the instance of the class
	public static MyDataBase getMyDataBaseInstance() {
		if (myDataBase == null) {
			System.out.println((count +1) + " st time loading :");
			return myDataBase = new MyDataBase();
		} else {
			count = count +1;
			System.out.println((count +1)+ "  time :");
			return myDataBase;
		}
	}
	@Override
	public String toString() {
		return "MyDataBase [" + (driver != null ? "driver=" + driver + ", " : "")
				+ (userName != null ? "userName=" + userName + ", " : "")
				+ (password != null ? "password=" + password : "") + "]";
	}
}
