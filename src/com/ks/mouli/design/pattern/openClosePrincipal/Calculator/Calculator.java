package com.ks.mouli.design.pattern.openClosePrincipal.Calculator;

import java.security.InvalidParameterException;

public class Calculator {

    public void calculate(CalculatorOperation operation, GenericeCalculatorOperation operation2) {
        approchOld(operation);
        approach2(operation2);
    }

	private void approach2(GenericeCalculatorOperation operation2) {
		if (operation2 == null) {
            throw new InvalidParameterException("Cannot perform operation");
        }
        operation2.perform();
	}

	private void approchOld(CalculatorOperation operation) {
		if (operation == null) {
            throw new InvalidParameterException("Can not perform operation");
        }

        if (operation instanceof Addition) {
            Addition addition = (Addition) operation;
            addition.setResult(addition.getLeft() + addition.getRight());
        } else if (operation instanceof Subtraction) {
            Subtraction subtraction = (Subtraction) operation;
            subtraction.setResult(subtraction.getLeft() - subtraction.getRight());
        }
	}
}