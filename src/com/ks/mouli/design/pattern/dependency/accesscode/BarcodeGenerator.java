package com.ks.mouli.design.pattern.dependency.accesscode;

import java.util.Random;

public class BarcodeGenerator implements AccesscodeGeneartor {

	@Override
	public String generateAccesscode() {
		return String.valueOf(new Random().nextLong());
	}

}
