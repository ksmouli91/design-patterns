package com.ks.mouli.design.pattern.dependency.accesscode;

import java.util.UUID;

public class QrCodeGenerator implements AccesscodeGeneartor {

	@Override
	public String generateAccesscode() {
		return String.valueOf(UUID.randomUUID());
	}

}
