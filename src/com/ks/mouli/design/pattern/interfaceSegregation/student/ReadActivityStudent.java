package com.ks.mouli.design.pattern.interfaceSegregation.student;

public interface ReadActivityStudent {

	void read();
}
