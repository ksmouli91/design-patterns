package com.ks.mouli.design.pattern.creational.factory;

public class BuildingFactory {
	public Building getBuilding(String buildingType) {
		if (buildingType != null) {
			if ("HOME".equalsIgnoreCase(buildingType)) {
				return new Home();
			} else if ("HOUSE".equalsIgnoreCase(buildingType)) {
				return new House();
			} else if ("HUT".equalsIgnoreCase(buildingType)) {
				return new Hut();
			}
		}
		return null;
	}

	public static void main(String[] args) {
		BuildingFactory buildingFactory = new BuildingFactory();
		String buildingType = null;
		if (args.length > 0) {
			buildingType = args [0];
		}
		Building building = buildingFactory.getBuilding(buildingType);
		building.build();
	}
}