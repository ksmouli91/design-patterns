package com.ks.mouli.design.pattern.behaviour.visitor;

public interface ShoppingCartVisitor {

	int visit(Book book);

	int visit(Fruit fruit);
}