package com.ks.mouli.design.pattern.openClosePrincipal.vechile;

public class Vehicle {

	public double vehicleNumber(Vehicle vcl) {
		if (vcl instanceof Car) {
			return ((Car) vcl).getNoOfWheels();
		} else if (vcl instanceof Bike) {
			return ((Bike) vcl).getNoOfWheels();
		} else {
			return 0;
		}
	}

}
