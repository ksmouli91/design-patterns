package com.ks.mouli.design.pattern.behaviour.command;

public class WorkingTimeCommand implements Command{
	Laptop laptop;
    public WorkingTimeCommand(Laptop laptop)
    {
        this.laptop = laptop;
    }
    public void execute()
    {
    	laptop.on();
    	laptop.work();
    }
}
