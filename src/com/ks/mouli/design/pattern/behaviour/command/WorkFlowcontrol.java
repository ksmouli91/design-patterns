package com.ks.mouli.design.pattern.behaviour.command;

class WorkFlowcontrol {
	Command workFlow;

	public WorkFlowcontrol() {
	}

	public void setCommand(Command command) {
		workFlow = command;
	}

	public void startActivity() {
		workFlow.execute();
	}
}