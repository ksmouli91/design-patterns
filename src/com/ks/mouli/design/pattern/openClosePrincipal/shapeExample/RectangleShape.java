package com.ks.mouli.design.pattern.openClosePrincipal.shapeExample;

public class RectangleShape implements Shape {
    private double length;
    private double height; 

    @Override
    public double calculateArea() {
        return length * height;
    }

}
