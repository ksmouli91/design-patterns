package com.ks.mouli.design.pattern.dependency.accesscode;

public class AccesscodeService {
	
	private BarcodeGenerator barcode = new BarcodeGenerator();
	
	public String createAcccessCode() {
		return barcode.generateAccesscode();
	}
	
}
