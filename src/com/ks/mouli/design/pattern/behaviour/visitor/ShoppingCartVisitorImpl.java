package com.ks.mouli.design.pattern.behaviour.visitor;

public class ShoppingCartVisitorImpl implements ShoppingCartVisitor {

	@Override
	public int visit(Book book) {
		int cost = 0;
		// apply 5$ discount if book price is greater than 50
		if (book.getPrice() > 50) {
			cost = book.getPrice() - 5;
		} else
			cost = book.getPrice();
		System.out.println("Book ISBN::" + book.getIsbnNumber() + " cost =" + cost);
		return cost;
	}

	@Override
	public int visit(Fruit fruit) {
		int cost = fruit.getPricePerKg() * fruit.getWeight();
		// apply 5$ charges if weight > 5 kgs
		if (fruit.getWeight() > 5) {
			cost = cost + 5;
		}
		System.out.println(fruit.getName() + " cost = " + cost);
		return cost;
	}

}