package com.ks.mouli.design.pattern.singleResponsibility.employeeExmaple;

public class CreditEmployeeSalary {

	public Long employeeId;
	
	public Double salary = 0.0;
	
	public void creditSalary(Double salary) {
		double totalBal = this.salary + salary;
		System.out.println(totalBal);
	}
}
