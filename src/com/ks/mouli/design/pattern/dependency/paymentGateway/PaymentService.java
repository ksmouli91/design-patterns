package com.ks.mouli.design.pattern.dependency.paymentGateway;

public class PaymentService {

	private GpayPayment payment = new GpayPayment();
	
	public String processPayment() {
		return payment.processPayment();
	}
	
}
