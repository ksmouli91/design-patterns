package com.ks.mouli.design.pattern.behaviour.template;

public class SocialWebSiteBuildMain {

	public static void main(String[] args) {
		SocialWebiteCommonTemplate commonTemplate = new FaceBook();
		commonTemplate.buildWebsite("facebook");
		
		commonTemplate = new WhatsApp();
		commonTemplate.buildWebsite("whatsApp");
	}

}
