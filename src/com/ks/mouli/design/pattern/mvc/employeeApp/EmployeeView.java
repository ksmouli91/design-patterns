package com.ks.mouli.design.pattern.mvc.employeeApp;

import java.util.List;

public class EmployeeView {

	public void showEmpoyeeDetails(List<EmployeeModel> employees) {
		for (EmployeeModel employee : employees) {
			System.out.println(employee.name);
			System.out.println(employee.id);
			System.out.println(employee.salary);
			System.out.println(employee.desgination);
		}
		
	}
	
}
