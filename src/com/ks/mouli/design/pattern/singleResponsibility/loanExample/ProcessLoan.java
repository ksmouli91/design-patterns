package com.ks.mouli.design.pattern.singleResponsibility.loanExample;

public class ProcessLoan {

	public void uploadDocument() {
		System.out.println("documents are uploaded ");
	}
	
	public void backgroundCheck() {
		System.out.println("backgroup pass");
	}

	public void creditAmout() {
		System.out.println("backgroup pass");
	}
	
	
}
