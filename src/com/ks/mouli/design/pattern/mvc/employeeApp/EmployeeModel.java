package com.ks.mouli.design.pattern.mvc.employeeApp;

public class EmployeeModel {

	public Long id;
	
	public String name;
	
	public Double salary;
	
	public String desgination;

	public EmployeeModel(Long id, String name, Double salary, String desgination) {
		super();
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.desgination = desgination;
	} 
	
}
