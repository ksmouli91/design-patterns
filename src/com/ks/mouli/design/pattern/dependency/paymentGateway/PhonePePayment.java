package com.ks.mouli.design.pattern.dependency.paymentGateway;

public class PhonePePayment {

	protected String  processPayment() {
		return "done payment with phonepe";
	}
}
