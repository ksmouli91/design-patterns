package com.ks.mouli.design.pattern.openClosePrincipal.shapeExample;

public class CommonAreaCalculator {
	public double calculateArea(Shape[] shapes) {
		double area = 0;
		for (Shape shape : shapes) {
			area = area + shape.calculateArea();
		}
		return area;
	}
}