package com.ks.mouli.design.pattern.behaviour.command;
interface Command
{
    public void execute();
}